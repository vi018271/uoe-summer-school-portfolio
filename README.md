This portfolio contains three pieces of work that I am most proud of, my A-Level Project where I created a Routing Application for Electric Cars, a text based adventure game in C which I created for my Autumn Coursework in my Programming in C/C++ Module and finally, a text based adventure game in C++ which I created for my Spring Coursework in the Programming in C/C++ Module.  It also contians the group project that I completed in my first year at university where we created a games website for children with learning difficulties.

You may not understand the function or objectives in the text based adventure games but hopefully you will be able to assess my competency in coding in C and C++.

Thank you very much.

Lewis Thackeray
thackeray.lewis0@gmail.com

