from tkinter import *
from tkinter import messagebox
from PIL import Image, ImageTk
import hashlib
import sqlite3
from tkinter import simpledialog
import tkintermapview
import re
import csv
import openrouteservice
from openrouteservice.directions import directions

# CREATING THE HASH ALGORITHM

def hash_string(passedPassword):
    global num_val
    myStr = passedPassword.encode('utf-8') # This line encodes the input from the user so it is an acceptable input into the hash function.
    num_val = hashlib.md5(myStr).hexdigest() # This line hashes the string.
    return num_val # This line returns the result of the hash function to the main program.

# CREATING THE SIGN-UP WINDOW

def sign_up():
    global newUsernameEnt
    global newPasswordEnt
    
    rootSignUp = Tk()
    rootSignUp.title("Sign-Up")
    rootSignUp.geometry("400x100")
    rootSignUp.configure(bg="#fff")
    rootSignUp.resizable(False,False)
    
    Label(rootSignUp,text="Please enter your new username:",fg="black",bg="white",font=("Microsoft YaHei UI Light",12)).place(x=5,y=5)
    newUsernameEnt = Entry(rootSignUp,width=25,fg="black",bg="white",font=("Microsoft YaHei UI Light",11))
    newUsernameEnt.place(x=200,y=5)
    newUsernameEnt.insert(0,"Username")
    
    Label(rootSignUp,text="Please enter your new passsword:",fg="black",bg="white",font=("Microsoft YaHei UI Light",12)).place(x=5,y=35)
    newPasswordEnt = Entry(rootSignUp,width=25,fg="black",bg="white",font=("Microsoft YaHei UI Light",11))
    newPasswordEnt.place(x=200,y=35)
    newPasswordEnt.insert(0,"Password")
    
    Button(rootSignUp,text="Create Account",command=operations).place(x=102,y=65)

# HASHING THE PASSWORD AND ADDING THE USERNAME AND PASSWORD TO A DATABASE

def operations():
    try:
        stringUsername = newUsernameEnt.get()
        stringPassword = newPasswordEnt.get()
        if (stringUsername == "Username") or (stringPassword == "Password"):
            messagebox.showinfo("Input String", "Please Enter a Valid Username and Password")
        elif (stringUsername != "Username") or (stringPassword != "Password"):
            passedPassword = stringPassword
            hashVal = hash_string(passedPassword) # Hashes the password and stores in a variable.
            # Connecting to the database:
            myDB_signUp = 'userLogins.db'
            conn_signUp = sqlite3.connect(myDB_signUp)
            c_signUp = conn_signUp.cursor()
            c_signUp.execute("""SELECT * FROM usernamesAndPasswords WHERE username=?""",(stringUsername,))
            signRow = c_signUp.fetchone()
            if signRow != None:
                messagebox.showinfo("Input String", "This Username already exists, please enter a different one!")
            if signRow == None:
                c_signUp.execute('INSERT INTO usernamesAndPasswords (username, password) VALUES (?, ?)', (stringUsername, hashVal))
                conn_signUp.commit()
                conn_signUp.close()
                messagebox.showinfo("Input String", "Your all signed up! Please now Sign-In")    
    except IOError:
        print(" ")

def login():
    username = userNameEnt.get()
    password = passwordEnt.get()
    # Hashing the password entered by the user.
    hashedPassword = hash_string(password)
    # Reading the hashed password associated with the username from the database.
    myDB_login = 'userLogins.db'
    conn_login = sqlite3.connect(myDB_login)
    c_login = conn_login.cursor()
    c_login.execute("""SELECT password FROM usernamesAndPasswords WHERE username=?""",(username,))
    loginRow = c_login.fetchone()
    if loginRow == None:
        messagebox.showinfo("Input String", "This username doesn't exist. Try again.")
    if loginRow != None:
        DBHashRowTemp = str(loginRow)
        DBHashRow = DBHashRowTemp.split("'")
        DBHash = DBHashRow[1]
        if DBHash == hashedPassword:
            set_up()
        elif DBHash != hashedPassword:
            messagebox.showinfo("Input String", "The username and/or password is incorrect. Try again.")


'''MAIN PROGRAM'''    

# CREATING THE WINDOW 

rootLogIn = Tk()
rootLogIn.title("Sign-In")
rootLogIn.geometry("925x500+300+200")
rootLogIn.configure(bg="#fff")
rootLogIn.resizable(False,False)

# CREATING THE BACKGROUND IMAGE

image = Image.open("ChargeNav.png")
resizeImage = image.resize((450,400))
img = ImageTk.PhotoImage(resizeImage)

label = Label(image=img)
label.image = img
label.place(x=0,y=5)

# CREATING THE FRAME

frame = Frame(rootLogIn,width=350,height=350,bg="white")
frame.place(x=480,y=70)

# CREATING THE HEADING

heading = Label(frame,text="Sign-In",fg="#57a1f8",bg="white",font=("Microsoft YaHei UI Light",46,'bold'))
heading.place(x=100,y=5)

# GETTING THE USERNAME

userNameEnt = Entry(frame,width=25,fg="black",bg="white",font=("Microsoft YaHei UI Light",11))
userNameEnt.place(x=30,y=80)
userNameEnt.insert(0,"Username")

Frame(frame,width=295,height=2,bg="black").place(x=25,y=107)

# GETTING THE PASSWORD

passwordEnt = Entry(frame,width=25,fg="black",bg="white",font=("Microsoft YaHei UI Light",11))
passwordEnt.place(x=30,y=150)
passwordEnt.insert(0,"Password")

Frame(frame,width=295,height=2,bg="black").place(x=25,y=177)

# CREATING THE SUBMIT BUTTON

Button(frame,width=39,pady=7,text="Sign-In",command=login).place(x=35,y=204)

# CREATING THE REGISTER BUTTON

registerLabel = Label(frame,text="Don't have an Account?",fg="black",bg="white",font=("Microsoft YaHei UI Light",12))
registerLabel.place(x=75,y=270)

registerButton = Button(frame,width=6,text="Sign Up",command=sign_up).place(x=215,y=270)
