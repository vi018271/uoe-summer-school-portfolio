from tkinter import *
from tkinter import simpledialog
import tkintermapview
import re
import csv
import sqlite3
import openrouteservice
from openrouteservice.directions import directions
import hashlib

# SUBROUTINES

# Validating the Postcode the User Enters       

def validate_postcode(input):
    return bool(re.match(r"^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$", input))

# Hash Algorithm

def hash_string(passedPassword):
    global num_val
    myStr = passedPassword.encode('utf-8') # This line encodes the input from the user so it is an acceptable input into the hash function.
    num_val = hashlib.md5(myStr).hexdigest() # This line hashes the string.
    return num_val # This line returns the result of the hash function to the main program.

def routing():
    origin = originEnt.get()
    destination = destinationEnt.get()
    
    valid_origin = validate_postcode(origin)
    valid_destination = validate_postcode(destination)
    
    if (valid_origin == True) and (valid_destination == True):
        routingFile = open("ukpostcodes.csv", "r")
        for routingLine in routingFile:
            if origin in routingLine:
                routingArrayOrigin = routingLine.split(",")
                originLat = float(routingArrayOrigin[2])
                originLongTemp = routingArrayOrigin[3]
                originLong = float(originLongTemp[:-2])
                originMarker = myMap.set_marker(originLat, originLong)
            if destination in routingLine:
                routingArrayDestination = routingLine.split(",")
                destinationLat = float(routingArrayDestination[2])
                destinationLongTemp = routingArrayDestination[3]
                destinationLong = float(destinationLongTemp[:-2])
                destinationMarker = myMap.set_marker(destinationLat, destinationLong)
           

    coords = ((originLong,originLat),(destinationLong,destinationLat))

    client = openrouteservice.Client(key='5b3ce3597851110001cf62480c7d62deacb64f4ca8a381875aa27ae6') # Specify your personal API key
    routes = str(directions(client, coords)) # Now it shows you all arguments for .directions

    instructions = routes.split("'distance'") # SPLITS THE ARRAY WHEN 'distance' APPEARS
    
    introductionLabel = Label(root1, text="Here are the directions to your destination:")
    
    for line in instructions:
        if "instruction" in line:
            myArrayRouting = line.split(":")
            directionTemp = myArrayRouting[4]
            direction = directionTemp[:-8]
            directionLabel = Label(root1, text=direction)
            directionLabel.pack()
            
    myMap.set_path([originMarker.position, destinationMarker.position])
    
    destinationsFullList = []
    destinationsFullList.append(origin)
    destinationsFullList.append(destination)
    destinationsFullList.append(destinationLat)
    destinationsFullList.append(destinationLong)
    
    destinationFile = open("destinationFile.csv", "a")
    destinationWriter = csv.writer(destinationFile)
    destinationWriter.writerow(destinationsFullList)
    destinationFile.close()
                
def routingSystem():
    
    global originEnt
    global destinationEnt
    global root1
    
    root1 = Tk()
    root1.title("Routing System")
    root1.geometry("600x900") # Width by Height
    
    originLab = Label(root1,text="Please enter your Origin Postcode: ")
    originEnt = Entry(root1)
    
    destinationLab = Label(root1,text="Please enter your Destination Postcode: ")
    destinationEnt = Entry(root1)
    
    submitButton = Button(root1,text="Submit",command=routing,height=2,width=8)
    
    originLab.pack()
    originEnt.pack()
    destinationLab.pack()
    destinationEnt.pack()
    submitButton.pack()

def plot_route():
    routingSystem()

def previously_travelled_routes():
    
    global root2
    
    root2 = Tk()
    root2.title("Previously Travelled Routes")
    root2.geometry("250x800")
    
    destinationFileRead = open("destinationFile.csv", "r")
    destinationNameLabel = Label(root2,text="Destinations")
    destinationNameLabel.grid(row=0, column=0)
    x = 1
    for destinationFileLine in destinationFileRead: 
        destinationFileLineArray = destinationFileLine.split(",") 
        destinationFromFile = destinationFileLineArray[1]
        destinationFromFileLabel = Label(root2, text=destinationFromFile)
        plotRouteButton = Button(root2, height=2, width=10, text="Plot Route", command = plot_route)
        x = x + 1 # Every loop the destination and the button will be on a different row.
        destinationFromFileLabel.grid(row=x,column=0)
        plotRouteButton.grid(row=x,column=1)   

def click_me():
    try:
        messagebox.showinfo("Input String", "This rating is outside the range!")
        ratingsList = []
        ratingPostcode = ratingPostcodeEnt.get() 
        rating = ratingEnt.get()
        ratingName = ratingNameEnt.get()
            
        myDB_rating = 'chargersDB.db'

        conn_rating = sqlite3.connect(myDB_rating)
        c_rating = conn_rating.cursor()
            
        c_rating.execute("""SELECT field3, field4, field5 FROM nationalChargePointRegistry WHERE field15 =?""",(ratingPostcode,))
        rows_rating = c_rating.fetchall()
        ratingsListTemp = str(rows_rating)
        ratingsList1 = ratingsListTemp.split("'")
        clickMeLat = float(ratingsList1[3])
        clickMeLong = float(ratingsList1[5])
        ratingsFullList = [ratingsList1[1], clickMeLat, clickMeLong, ratingPostcode, rating, ratingName]
        ratingFile = open("ratings.csv", "a")
        ratingWriter = csv.writer(ratingFile)
        ratingWriter.writerow(ratingsFullList)
        ratingFile.close()
    except:
        if IOError:
            print(" ")

def update_details():
    userUsername = userUsernameEnt.get()
    oldPassword = oldPasswordEnt.get()
    updatedPassword = updatedPasswordEnt.get()
    hashedOldPassword = hash_string(oldPassword)
    detailsDB = 'userLogins.db'
    detailsconn = sqlite3.connect(detailsDB)
    detailsc = detailsconn.cursor()
    detailsc.execute("""SELECT password FROM usernamesAndPasswords WHERE username=?""",(userUsername,))
    detailsRow = detailsc.fetchone()
    if detailsRow == None:
        messagebox.showinfo("Input String", "This username doesn't exist. Try again.")
    if detailsRow != None:
        DBHashRowTemp = str(detailsRow)
        DBHashRow = DBHashRowTemp.split("'")
        DBHash = DBHashRow[1]
        if DBHash == hashedOldPassword:
            # UPDATE PASSWORD HERE
            hashedNewPassword = hash_string(updatedPassword)
            detailsc.execute("""UPDATE usernamesAndPasswords SET password=? WHERE username=?""",(hashedNewPassword, userUsername,))
            detailsconn.commit()
            detailsconn.close()
            messagebox.showinfo("Input String", "Your Password has been changed!")
        elif DBHash != hashedOldPassword:
            messagebox.showinfo("Input String", "The username and/or password is incorrect. Try again.")
    
def changeDetailsUI():
    
    global userUsernameEnt
    global oldPasswordEnt
    global updatedPasswordEnt
    
    detailsRoot = Tk()
    detailsRoot.title("Change Details")
    detailsRoot.geometry("400x200") # Width by Height
    
    userUsernameLab = Label(detailsRoot,text="Please enter your Username: ")
    userUsernameEnt = Entry(detailsRoot)
    
    oldPasswordLab = Label(detailsRoot,text="Please enter your Current Password: ")
    oldPasswordEnt = Entry(detailsRoot)
    
    updatedPasswordLab = Label(detailsRoot,text="Please enter your New Password: ")
    updatedPasswordEnt = Entry(detailsRoot)
    
    submitNewDetails = Button(detailsRoot,text="Submit",command=update_details,height=2,width=8)
    
    userUsernameLab.pack()
    userUsernameEnt.pack()
    oldPasswordLab.pack()
    oldPasswordEnt.pack()
    updatedPasswordLab.pack()
    updatedPasswordEnt.pack()
    submitNewDetails.pack()
    
def sign_out():
    messagebox.showinfo("Input String", "Thanks for using ChargeNav!")
    exit()
    
def change_current_location():
    get_current_location()

def deleteAcc():
    closeAccUserName = usernameEntClose.get()
    myDB_closeAcc = 'userLogins.db'
    conn_closeAcc = sqlite3.connect(myDB_closeAcc)
    c_closeAccount = conn_closeAcc.cursor()
    c_closeAccount.execute("""SELECT password FROM usernamesAndPasswords WHERE username=?""",(closeAccUserName,))
    closeRow = c_closeAccount.fetchone()
    if closeRow == None:
        messagebox.showinfo("Input String", "This username doesn't exist. Try again.")
    if closeRow != None:
        c_closeAccount.execute("""DELETE FROM usernamesAndPasswords WHERE username==?""",(closeAccUserName,))
        closeRowTemp = c_closeAccount.fetchall()
        messagebox.showinfo("Input String", "Your account details have been deleted.")
        

def close_account():
    global usernameEntClose
    
    closeRoot = Tk()
    closeRoot.title('Close Account')
    closeRoot.geometry("400x200")
    
    closeLabel = Label(closeRoot,text="Please enter your new username:")
    closeLabel.pack()
    usernameEntClose = Entry(closeRoot)
    usernameEntClose.pack()

    Button(closeRoot,text="Check Username",command=deleteAcc).pack()

def settings_window():
    global settingsRoot
    settingsRoot = Tk()
    settingsRoot.title('Settings Window')
    settingsRoot.geometry("400x200")
    changeDetailsButton = Button(settingsRoot,text="Change Password",height=2,width=24,command=changeDetailsUI)
    changeDetailsButton.pack()
    signOutButton = Button(settingsRoot,text="Sign Out",height=2,width=24,command=sign_out)
    signOutButton.pack()
    changeCurrentLocationButton = Button(settingsRoot,text="Change Current Location",height=2,width=24,command = change_current_location)
    changeCurrentLocationButton.pack()
    closeAccountButton = Button(settingsRoot, text="Close Account", height=2, width=24, command = close_account)
    closeAccountButton.pack()
    
# Subroutine to Get the Users Current Location

def get_current_location():
    postcode = simpledialog.askstring(title="Input String", prompt="Please Enter your Postcode in Capitals")
    #Checking if the postcode is valid
    valid = validate_postcode(postcode)
    if valid == True:
        #Destroying Pop-Up Box when Postcode is Entered
        postcodeButton.destroy()
        #Open the TXT File
        postcodeFile = open("ukpostcodes.csv", "r")
        for line in postcodeFile:
            #Read through line by line till postcode is found
            if postcode in line:
                myArray = line.split(",")
                lat = myArray[2] #Latitude of Postcode
                long = myArray[3] #Longitude of Postcode
                x = float(lat)
                y = float(long)
                currentLocation = myMap.set_marker(x,y, text="Current Location") #Setting a Marker at the User's Current Location
                address = myArray[1]
                userAddress = str(address)
                myMap.set_address(userAddress) #Setting a Address at the User's Current Location
                return lat,long #Returning the Latitude and Longitude Coordinates into the Main Program
    else:
        messagebox.showinfo("Input String", "Please Enter a Valid UK Postcode")

# Filtering the Charging Points depedning on Option Selected by the User

def drop_selection(selection):
    if selection == "DC":
        myDB_DC = 'chargersDB.db'
        #CONNECTION
        conn_DC = sqlite3.connect(myDB_DC)
        c_DC = conn_DC.cursor()
        # PRINTING WHAT IS IN THE COLUMN AND CREATING A NEW MARKER
        c_DC.execute("SELECT field4, field5 FROM nationalChargePointRegistry WHERE field76!='Single Phase AC' OR 'Three Phase AC';")
        rows_DC = c_DC.fetchall()
        myList_DC = []
        for row_DC in rows_DC:
            try:
                latAndLong_DC = str(row_DC)
                myList_DC = latAndLong_DC.split("'")
                if "." in myList_DC[1]:
                    DCLatitude = float(myList_DC[1])
                    DCLongitude = float(myList_DC[3])
                    myMap.set_marker(DCLatitude, DCLongitude, marker_color_circle="blue", marker_color_outside="blue")
            except:
                if IOError:
                    print(" ")     
        messagebox.showinfo("Input String", "All DC Chargepoints are Shown in Blue")
    if selection == "AC":
        myDB_AC = 'chargersDB.db'
        #CONNECTION
        conn_AC = sqlite3.connect(myDB_AC)
        c_AC = conn_AC.cursor()
        # PRINTING WHAT IS IN THE COLUMN AND CREATING A NEW MARKER
        c_AC.execute("SELECT field4, field5 FROM nationalChargePointRegistry WHERE field76 ='Single Phase AC' OR 'Three Phase AC';")
        rows_AC = c_AC.fetchall()
        myList_AC = []
        for row_AC in rows_AC:
            try:
                latAndLong_AC = str(row_AC)
                myList_AC = latAndLong_AC.split("'")
                if "." in myList_AC[1]:
                    ACLatitude = float(myList_AC[1])
                    ACLongitude = float(myList_AC[3])
                    myMap.set_marker(ACLatitude, ACLongitude, marker_color_circle="green", marker_color_outside="green")
            except:
                if IOError:
                    print(" ")     
        messagebox.showinfo("Input String", "All AC Chargepoints are Shown in Green")
    if selection == "Rating":
        valid_rating = True
        while valid_rating == True:
            choice_rating = simpledialog.askstring(title="Input String", prompt="Please Enter the Rating you desire")
            if choice_rating >= "1" and choice_rating <= "6":
                valid_rating = False
                if choice_rating == "1":
                    rating_file_read = open("ratings.csv", "r")
                    for readChoiceRow in rating_file_read:
                        readChoiceRowArray1 = readChoiceRow.split(",")
                        readChoiceRowRatingTemp = int(readChoiceRowArray1[4])
                        if readChoiceRowRatingTemp == 1:
                            xLat1 = float(readChoiceRowArray1[1])
                            yLong1 = float(readChoiceRowArray1[2])
                            myMap.set_marker(xLat1,yLong1,marker_color_circle="orange", marker_color_outside="orange")
                        elif readChoiceRowRatingTemp != 1:
                             print(" ")
                    messagebox.showinfo("Input String", "Chargepoints with a Rating of 1 are shown in Orange")
        
                if choice_rating == "2":
                    rating_file_read2 = open("ratings.csv", "r")
                    for readChoiceRow2 in rating_file_read2:
                        readChoiceRowArray21 = readChoiceRow2.split(",")
                        readChoiceRowRatingTemp2 = int(readChoiceRowArray21[4])
                        if readChoiceRowRatingTemp2 == 2:
                            xLat2 = float(readChoiceRowArray21[1])
                            yLong2 = float(readChoiceRowArray21[2])
                            myMap.set_marker(xLat2,yLong2,marker_color_circle="blue", marker_color_outside="blue")
                        elif readChoiceRowRatingTemp2 != 2:
                             print(" ")
                    messagebox.showinfo("Input String", "Chargepoints with a Rating of 2 are shown in Blue")

                if choice_rating == "3":
                    rating_file_read3 = open("ratings.csv", "r")
                    for readChoiceRow3 in rating_file_read3:
                        readChoiceRowArray31 = readChoiceRow3.split(",")
                        readChoiceRowRatingTemp3 = int(readChoiceRowArray31[4])
                        if readChoiceRowRatingTemp3 == 3:
                            xLat3 = float(readChoiceRowArray31[1])
                            yLong3 = float(readChoiceRowArray31[2])
                            myMap.set_marker(xLat3,yLong3,marker_color_circle="yellow", marker_color_outside="yellow")
                        elif readChoiceRowRatingTemp3 != 3:
                             print(" ")
                    messagebox.showinfo("Input String", "Chargepoints with a Rating of 3 are shown in Yellow")
                if choice_rating == "4":
                    rating_file_read4 = open("ratings.csv", "r")
                    for readChoiceRow4 in rating_file_read4:
                        readChoiceRowArray41 = readChoiceRow4.split(",")
                        readChoiceRowRatingTemp4 = int(readChoiceRowArray41[4])
                        if readChoiceRowRatingTemp4 == 4:
                            xLat4 = float(readChoiceRowArray41[1])
                            yLong4 = float(readChoiceRowArray41[2])
                            myMap.set_marker(xLat4,yLong4,marker_color_circle="purple", marker_color_outside="purple")
                        elif readChoiceRowRatingTemp4 != 4:
                             print(" ")
                    messagebox.showinfo("Input String", "Chargepoints with a Rating of 4 are shown in Purple")
                if choice_rating == "5":
                    rating_file_read5 = open("ratings.csv", "r")
                    for readChoiceRow5 in rating_file_read5:
                        readChoiceRowArray51 = readChoiceRow5.split(",")
                        readChoiceRowRatingTemp5 = int(readChoiceRowArray51[4])
                        if readChoiceRowRatingTemp5 == 5:
                            xLat5 = float(readChoiceRowArray51[1])
                            yLong5 = float(readChoiceRowArray51[2])
                            myMap.set_marker(xLat5,yLong5,marker_color_circle="green", marker_color_outside="green")
                        elif readChoiceRowRatingTemp5 != 5:
                             print(" ")
                    messagebox.showinfo("Input String", "Chargepoints with a Rating of 5 are shown in Green")
            else:
                valid_rating = True
                messagebox.showinfo("Input String", "Please enter a Valid Rating")
    if selection == "24/7":
        myDB_24 = 'chargersDB.db'
        #CONNECTION
        conn_24 = sqlite3.connect(myDB_24)
        c_24 = conn_24.cursor()
        c_24.execute("SELECT field4, field5 FROM nationalChargePointRegistry WHERE field56='0'")
        rows_24 = c_24.fetchall()
        for row_24 in rows_24:
            temp24 = str(row_24)
            myArray24 = temp24.split("'")
            Lat24 = float(myArray24[1])
            Long24 = float(myArray24[3])
            myMap.set_marker(Lat24,Long24,marker_color_circle="orange", marker_color_outside="orange")
        messagebox.showinfo("Input String", "Chargepoints that are available 24/7 are shown in Orange")
        
    if selection == "Previously Visited":
        previouslyVisitedFile = open("destinationFile.csv", "r")
        for previouslyVisitedLine in previouslyVisitedFile:
            previouslyVisitedArray = previouslyVisitedLine.split(",")
            previouslyVisitedDestination = previouslyVisitedArray[1]
            previouslyVisitedLatitude = float(previouslyVisitedArray[2])
            previouslyVisitedLongitude = float(previouslyVisitedArray[3])
            validPreviousDestination = validate_postcode(previouslyVisitedDestination)
            if validPreviousDestination == True:
                previouslyVisited_DB = 'chargersDB.db'
                conn_previouslyVisited = sqlite3.connect(previouslyVisited_DB)
                c_previouslyVisited = conn_previouslyVisited.cursor()
                c_previouslyVisited.execute("""SELECT field3 FROM nationalChargePointRegistry WHERE field15=?""",(previouslyVisitedDestination,))
                rows_PV = c_previouslyVisited.fetchall()
                if len(rows_PV) != 0:
                    myMap.set_marker(previouslyVisitedLatitude,previouslyVisitedLongitude,marker_color_circle="purple",marker_color_outside="purple")
                elif len(rows_PV) == 0:
                    print(" ")
            elif validPreviousDestination == False:
                print(" ")
        messagebox.showinfo("Input String", "Chargepoints that you have Previously Visited are shown in Purple")
            
        
#Filters Drop Down Menu

def filters_drop_menu():
    # Options for Dropdown Menu
    options = [
        "AC",
        "DC",
        "24/7",
        "Rating",
        "Previously Visited"
    ]
    # Datatype of Menu
    clicked = StringVar()
    # Default Value
    clicked.set("Filters")
    # Create Dropdown menu
    drop = OptionMenu(frame, clicked , *options, command=drop_selection )
    drop.grid(row=0, column=8)
    
def rating_system():
    
    ratingsRoot = Tk()
    ratingsRoot.title("Rating Window")
    ratingsRoot.geometry("500x300") # Width by Height
    
    global ratingPostcodeEnt
    global ratingEnt
    global ratingNameEnt

    ratingPostcodeLab = Label(ratingsRoot, text="Please enter the Postcode of the Charger you would like to review")
    ratingPostcodeEnt = Entry(ratingsRoot)

    ratingLab = Label(ratingsRoot, text="Please enter the rating you would like to give this ChargePoint out of 5")
    ratingEnt = Entry(ratingsRoot)

    ratingNameLab = Label(ratingsRoot, text="Please enter the name you would like to have associated with the Rating")
    ratingNameEnt = Entry(ratingsRoot)

    ratingButton = Button(ratingsRoot, text="Submit", command = click_me, height=2, width=8)

    ratingPostcodeLab.pack()
    ratingPostcodeEnt.pack()
    ratingLab.pack()
    ratingEnt.pack()
    ratingNameLab.pack()
    ratingNameEnt.pack()
    ratingButton.pack()
    
    return ratingsRoot

def helpWin():
    helpRoot = Tk()
    helpRoot.title("Help Window")
    helpRoot.geometry("500x1000") # Width by Height
    
    Label(helpRoot,text="How do I set my Current Location?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="When you open the application you will be greeted by a pop-up box asking you ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="to enter your postcode, this will then set this postcode as you current ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="location and will display it on the map.",font=('Helvetica', 6)).pack()
    
    Label(helpRoot,text="How do I rate a Charge Point?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To rate a Charge Point you should click on the Rating Button on the ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="Main Window and then fill in the Window in order to rate a Charge Point.",font=('Helvetica', 6)).pack()
    
    Label(helpRoot,text="How do I find AC Charge Points?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To find AC Charge Points you should click on the Filters Button and then ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="use the drop-down menu to select the AC Filter which will show AC Charge ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="Points on the map.",font=('Helvetica', 6)).pack()
    
    Label(helpRoot,text="How do I find DC Charge Points?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To find DC Charge Points you should click on the Filters Button and then ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="use the drop-down menu to select the DC Filter which will show DC Charge ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="Points on the map.",font=('Helvetica', 6)).pack()
    
    Label(helpRoot,text="How do I find Charge Points that are available 24/7?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To find Charge Points that are available 24/7 you should click on the Filters ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="Button and use the drop-down menu to select the 24/7 Filter which will ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="show Charge Points that are available 24/7 on the map.",font=('Helvetica', 6)).pack()
    
    Label(helpRoot,text="How do I find Charge Points with a Specific Rating?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To find Charge Points that have a Specific Rating you should click on the ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="Filters Button and use the drop-down menu to select the Rating Filter, then a ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="pop-up box will appear asking you to enter the desired rating 1-5 and then ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="Charge Points with that rating will be shown on the map.",font=('Helvetica', 6)).pack()
    
    Label(helpRoot,text="How do I find Charge Points that I have Previously Visited?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To find Charge Points that you have Previously Visited you should click on the ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="Filters Button and use the drop-down menu to select the Previously Visited Filter ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="then Charge Points that you have Previously Visited will be shown on the map.",font=('Helvetica', 6)).pack()
    
    Label(helpRoot,text="How do I set a New Route?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To set a New Route you should click on the New Route Button and enter you origin and ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="destination and then the directions will be displayed in the window and a line of best fit ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="will be drawn on the map between the origin and destination.",font=('Helvetica', 6))
    
    Label(helpRoot,text="How do I see my Previously Travelled Routes?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To see your Previously Travelled Routes you should click on the Previously Travelled ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="Routes Button, a window will then appear with all of the destinations you have Previously ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="Visited along with a button next to each one which allows you to plot a route to this destination.",font=('Helvetica', 6)).pack()
    
    Label(helpRoot,text="How do I Sign Out of the Application?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To Sign Out of ChargeNav you should click the Settings Button, a window will then appear with several ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="options on it, then click the Sign Out Button.",font=('Helvetica', 6)).pack()
    
    Label(helpRoot,text="How do I Change my Password?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To Change your Password you should click the Settings Button, a window will then appear with several ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="options on it, then click the Change Password Button where a window will appear and you should fill in ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="this window in order to Change your Password.",font=('Helvetica', 6)).pack()
    
    Label(helpRoot,text="How do I Change my Current Location?",font=('Helvetica', 12, 'bold')).pack()
    Label(helpRoot,text="To Change your Current Location you should click on the Settings Button, a window will then appear with ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="several options on it, then click the Change Current Location Button where a window will appear asking ",font=('Helvetica', 6)).pack()
    Label(helpRoot,text="you to enter a postcode which will then be set as your Current Location and will be displayed on the map.",font=('Helvetica', 6)).pack()

def exitSub():
    exit()

# MAIN PROGRAM

# Creating the window

root = Tk()
root.title('ChargeNav')
root.geometry("900x600") #Size of the Window Width by Height
root.configure(bg="#a6d7f5")

frame = Frame(root)
frame.pack()

my_label = LabelFrame(root)
my_label.pack(pady=20)

# Creating the map.

myMap = tkintermapview.TkinterMapView(my_label,width = 800,height = 600,corner_radius = 0)
myMap.set_zoom(100)
myMap.pack()

chargeFile = open("national-charge-point-registry.csv", "r")
csvChargeFileReader = csv.reader(chargeFile)
next(csvChargeFileReader)

# Putting the Charging Points on a Map

for row in chargeFile:
    try:
        chargeArray = row.split(",")
        chargePointLat = chargeArray[3]
        chargePointLong = chargeArray[4]
        if "." in chargePointLat:
            newX = float(chargePointLat)
            newY = float(chargePointLong)
            myMap.set_marker(newX, newY)
        else:
            next(csvChargeFileReader)
    except:
        if IOError:
            next(csvChargeFileReader)
                        
# Creating the Pop-Up Box

postcodeButton = Button(frame, text="Current Location", command = get_current_location, height=2, width=15)

#Creating the Buttons

filterButton = Button(frame, text="Filters", height=2, width=10, command = filters_drop_menu)
filterButton.grid(row=0, column=3)

helpButton = Button(frame, text="Help", height=2, width=10,command = helpWin)
helpButton.grid(row=0, column=2)

newRouteButton = Button(frame, text="New Route", height = 2, width=10, command = routingSystem)
newRouteButton.grid(row=0, column=4)

settingsButton = Button(frame, text="Settings", height = 2, width=10, command = settings_window)
settingsButton.grid(row=0, column=6)

ratingButton = Button(frame, text="Rating", height = 2, width=10, command=rating_system)
ratingButton.grid(row=0,column=1)

pt = Button(frame, text="Previously Travelled Routes", height = 2, width = 20, command=previously_travelled_routes)
pt.grid(row=0, column=5)

exitButton = Button(frame,text="Exit", height = 2, width = 10,command=exitSub)
exitButton.grid(row=0,column=7)

#Creating the variables lat and long in the main program

lat,long = get_current_location()

root.mainloop()