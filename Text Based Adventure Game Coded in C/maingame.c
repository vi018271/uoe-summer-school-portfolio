// we need the include files io and stdlib and string and stdbool and time
// we need to define a constant for the number of rooms
// we need to define a constant for the maximum number of connections per room
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

// we want to write a text based game that is a little bit like a choose your own adventure book
// each turn we will present the player with a choice of actions
// the player will choose an action and we will carry it out
// the player can look at the room, or move to another room

// we will need to keep track of the current room
// we need a way to represent the rooms
// we need a way to represent the connections between the rooms
// let's have a datastructure that represents the rooms
// room has connections and connections have rooms so we need to define connection first but we can't because room needs it
// we can use a forward declaration to tell the compiler that connection is a struct that will be defined later
typedef struct connection connection;
typedef struct room {
  char* name;
  char* description;
  connection* connections;
  int numConnections;
} room;

//making currentRoom a global variable in my program.

room* currentRoom;
room* finalRecommendationString;
room* roomName;

// we need a way to represent the connections between the rooms
// let's have a datastructure that represents the connections
typedef struct connection {
  room* room1;
  room* room2;
  room* room3; // Defining the third room that the user can move to.
} connection;

// we need a way to represent the player
// let's have a datastructure that represents the player
typedef struct player {
  room* currentRoom;
  room* finalRecommendationString;
} player;

// we need a way to represent the game
// let's have a datastructure that represents the game
typedef struct game {
  room* rooms;
  int numRooms;
  player* player;
} game;

// ADDING NEW FEATURES TO THE GAME: (2) THE SCORING SYSTEM

// The second feature that I am going to add to this game is a scoring system
// I am going to implement this scoring system based on how many of the 10 rooms that the user has visit

// FOR YOUR INFORMATION - I am only displaying the score to the user when they quit the game.
// I am doing this because I do not want to distract the user from the game.


// let's have a set of functions to get and calculate the current score
int calculateScore(int numLines) {
    // We have passed the number of lines in the file into the subroutine.
    // We need to calculate the score based on the number of lines in the file.
    // We need to create a variable to store the score.
    int score = 0;
    // We need to create a variable to store the number of rooms that the user has visited.
    int numRoomsVisited = numLines;
    // We need to calculate the score of the user.
    score = numRoomsVisited * 10;
    // We need to print the score to the user.
    printf("You have completed %d%% of the game!\n", score);
}

int getScore() {
    // We need to read the number of rooms in the visited.csv file.
    // We need to open the file
    FILE* score_file2 = fopen("visited.csv", "r");
    // we need to check that the file opened successfully
    if (score_file2 == NULL) {
        printf("Error opening file\n");
        exit(1);
    }
    // we need to count the number of lines in the file
    int numLines = 0;
    char line[500];
    while (fgets(line, 500, score_file2) != NULL) {
        numLines++;
    }
    // we need to rewind the file
    rewind(score_file2);
    // We need to pass the number of lines in the file into the calculateScore function.
    calculateScore(numLines);

}

// let's have a function to update the score
int update_score(char* passingVar) {
    // We have successfully passed the roomName into this function as passingVar.
    // The purpose of this subroutine is to update the score in the visited.csv file.

    // We need to open the file
    FILE* score_file1 = fopen("visited.csv", "a");
    // we need to check that the file opened successfully
    if (score_file1 == NULL) {
        printf("Error opening file\n");
        exit(1);
    }
    // We need to write the roomName to the file.
    fprintf(score_file1, "%s\n", passingVar);
    // BE AWARE - When we are writing the currentRoom to the file, the first room Enchanted Forest is not being written to the file.
    // This is because it acts as a start point for the program.  If they want to complete the game they have to come back to the Enchanted Forest.
    // We need to close the file
    fclose(score_file1);
}

// For ease I am going to create a function which checks if the room name is in the file.

// let's have a function to check if a string is in a file
int isStringInFile(const char *filename, const char *targetString) {
    FILE *file = fopen(filename, "r");

    // Check if the file was successfully opened
    if (file == NULL) {
        fprintf(stderr, "Error opening the file.\n");
        return 0; // Return false
    }

    char line[500];

    // Read each line from the file
    while (fgets(line, sizeof(line), file) != NULL) {
        // Remove the trailing newline character
        line[strcspn(line, "\n")] = '\0';

        // Check if the target string is present in the line
        if (strstr(line, targetString) != NULL) {
            // Close the file and return true
            fclose(file);
            return 1; // Return true
        }
    }

    // Close the file
    fclose(file);

    // Return false if the string is not found in the file
    return 0;
}

// let's have a function to verify the score
int verify_score(char* passingVar) {
    // We have successfully passed the roomName into this function as passingVar.
    // The purpose of this subroutine is to check if the passingVar is already in the file.

    // we need to define the filename and line in the file as constants so they do not change during execution.
    const char *filename = "visited.csv";
    const char *targetString = passingVar;

    // Check if the string is present in the file
    if (isStringInFile(filename, targetString)) {
        // If this condition is satisfied the roomName is already in the file.
    } else {
        // We need to call the update_score subroutine.
        update_score(passingVar);
    }
}

// let's have a function to print a menu and get user choice, return the choice
int getMenuChoice() {
  int choice;
  printf("What would you like to do?\n");
  printf("1. Look around\n");
  printf("2. Move to another room\n");
  printf("3. Quit\n");
  scanf("%d", &choice);
  // we need to check that the choice is valid
  while (choice < 1 || choice > 3) {
    printf("Invalid choice, please try again\n");
    scanf("%d", &choice);
  }
  return choice;
}

// I need to create a function that will provide a recommendation to the user.
// I will do this by creating a function, which will take in the current room that the user is in.
int recommendRoom(int roomNumber) {
    // Now we have got the roomNumber we need to find the most popular suggestion from that room
    // We have an array which stores the most popular suggestion for a given room where rec[0] returns the recommendation for room 1.

    // I was using a csv file to store the recommendations however I have decided to use an array as I could not get the csv file to work.
    // Here i am creating an array which stores the most popular suggestion for a given room.
    int recommendations[10] = {4,2,4,7,0,4,6,9,8,4}; // This is the array which stores the most popular suggestion for a given room.

    int finalRecommendation = recommendations[roomNumber - 1]; // This stores the final recommendation in a variable.
    // I am going to write the names of the different rooms in an array for easy access.
    char* roomNames[10] = {"Enchanted Forest", "Dragon's Lair", "Crystal Craven", "Witches' Den", "Elven Library", "Mermald Grotto", "Dwarven Forge", "Goblin Market", "Haunted Manor", "Temple of the Ancients"};
    // Now I need to store the room name of the final recommendation in a variable.
    char* finalRecommendationString = roomNames[finalRecommendation];
    // Now I need to print the recommendation to the user.
    printf("We recommend the next room you visit should be the %s.\n", finalRecommendationString);

}

// let's have a function to print the room description
void printRoomDescription(room* room) {
    // Printing the room variables to the user.
    char* roomName = room->name; // This gets the roomName that we are currently in.
    char* roomDescription = room->description; // This gives the description of the room we are currently in.

    // We need to pass the roomName into the verify_score subroutine.
    verify_score(roomName);

    printf("You are in the %s.\n", roomName);
    printf("%s\n", roomDescription);

    // ADDING NEW FEATURES TO THE GAME: (1) THE RECOMMENDATION SYSTEM

    // At the moment we have the roomName and roomDescription, however we now need to find the room number
    // To do this we need to open rooms.csv and read the room number from there.
    // We need to open the file
    FILE* file3 = fopen("rooms.csv", "r");
    // we need to check that the file opened successfully
    if (file3 == NULL) {
        printf("Error opening file\n");
        exit(1);
    }
    int numberOfLinesInFile3 = 10; // This is the number of lines in the file.
    // Now we need to loop through the file and examine each line.
    for (int i = 0; i < numberOfLinesInFile3; i++) {
        // We need to examine each line in the file to see if it contains the roomName.
        // We need to initialize the variables that we will use to read the file.
        char line[500];
        char* name1;
        char* endofname1;
        char* description1;
        char* endofdesc1;
        // we need to read the line
        fgets(line, 500, file3);
        // we need to remove the newline character
        line[strlen(line)-1] = '\0';
        // we need to split the line into the name and description
        // the strings are in quotation marks, and the two quoted strings are separated by a comma
        // we need to split the line on ", " (the four characters)
        //everything between the first and second " is the name
        //everything between the third and fourth " is the description
        // we need to find the first "
        name1 = strtok(line, "\"");
        // we need to find the second " and record where it is so we can null terminate the string
        endofname1=strtok(NULL, "\"");
        // we need to find the third "
        description1 = strtok(NULL, "\"");
        // we need to find the fourth "
        endofdesc1=strtok(NULL, "\0");
        //we need to be sure that name and description are null terminated strings
        name1[endofname1-name1]='\0';
        //description[endofdesc-description]='\0';
        // We need to check if the roomName is the same as the name1.
        if (strcmp(roomName, name1) == 0) {
            // We need to print the room number to the user.
            int roomNumber = i + 1; // This now stores the roomNumber in a variable
            // Now we need to open the recommendations.csv file and read the recommendations from there.
            // We need to open the file
            FILE* file4 = fopen("recommendations.csv", "r");
            // we need to check that the file opened successfully
            if (file4 == NULL) {
                printf("Error opening file\n");
                exit(1);
            }
            int numberOfLinesInFile4 = 10; // This is the number of lines in the file.
            // Now we need to loop through the file and examine each line.
            for (int a = 0; a < numberOfLinesInFile4; a++) {
                // We need to check if a + 1 is equal to the roomNumber.
                if (a + 1 == roomNumber) {
                   // We need to print a message to the user
                    recommendRoom(roomNumber); // This calls the function that will recommend a room to the user.
                    // We need to close the file
                    fclose(file4);
                }
            }
        }

    }
}

// a function to get user choice of room to move to
int getRoomChoice(room* currentRoom) {
  int choice;
  printf("Which room would you like to move to?\n");
  for (int i = 0; i < currentRoom->numConnections; i++) {
    printf("%d. %s\n", i+1, currentRoom->connections[i].room2->name);

  }
  scanf("%d", &choice);
  // we need to check that the choice is valid
  while (choice < 1 || choice > currentRoom->numConnections) {
    printf("Invalid choice, please try again\n");
    scanf("%d", &choice);
  }
  return choice;
}

// a function to move the player to another room, and describe it to the user
void movePlayer(player* player, int choice) {
    player->currentRoom = player->currentRoom->connections[choice - 1].room2; // This line updates the current room.
    printRoomDescription(player->currentRoom);
}

// a function to load the rooms from a file
// the file is called rooms.csv, and has a room name and room description on each line
// the function returns an array of rooms
room* loadRooms() {
  // open the file
  FILE* file = fopen("rooms.csv", "r");
  // we need to check that the file opened successfully
  if (file == NULL) {
    printf("Error opening file\n");
    exit(1);
  }
  // we need to count the number of lines in the file
  int numLines = 0;
  char line[500];
  while (fgets(line, 500, file) != NULL) {
    numLines++;
  }
  // we need to rewind the file
  rewind(file);
  // we need to allocate memory for the rooms
  room* rooms = malloc(sizeof(room) * numLines);
  // we need to read the rooms from the file
  for (int i = 0; i < numLines; i++) {
    // we need to read the line
    fgets(line, 500, file);
    // we need to remove the newline character
    line[strlen(line)-1] = '\0';
    // we need to split the line into the name and description
    // the strings are in quotation marks, and the two quoted strings are separated by a comma
    // we need to split the line on ", " (the four characters)
    //everything between the first and second " is the name
    //everything between the third and fourth " is the description
    // we need to find the first "
    char* name = strtok(line, "\"");
    // we need to find the second " and record where it is so we can null terminate the string
    char* endofname=strtok(NULL, "\"");


    // we need to find the third "
    char* description = strtok(NULL, "\"");
    // we need to find the fourth "
    char* endofdesc=strtok(NULL, "\0");
    //we need to be sure that name and description are null terminated strings
    name[endofname-name]='\0';
    //description[endofdesc-description]='\0';
    // we need to create a room
    room room;
    //we need to copy the string into the room name
    room.name = malloc(sizeof(char) * strlen(name) + 1);
    strcpy(room.name, name);
    //we need to copy the string into the room description
    room.description = malloc(sizeof(char) * strlen(description) + 1);
    strcpy(room.description, description);
    room.connections = NULL;
    room.numConnections = 0;
    // we need to add the room to the array
    rooms[i] = room;
  }
  // we need to close the file
  fclose(file);

  // we need to create a maze like set of connections between the rooms
  // we need to loop through the rooms
  // We need to open the connections.csv file and read the connections for each room from it.
    FILE* file2 = fopen("connections.csv", "r");
    // we need to check that the file opened successfully
    if (file2 == NULL) {
        printf("Error opening file\n");
        exit(1);
    }
    // we need to loop through the file and examine each line.
    for (int i = 0; i < numLines; i++) {
        // we need to initialize the variables that we will use to read the file.
        int numberOfConnections = 3;
        int roomToConnect;
        // we need to read the line
        fgets(line, 500, file2);
        // we need to remove the newline character
        line[strlen(line)-1] = '\0';
        // We need to split each line in the file into the different connections each room has.
        // We need to split the line on the commas
        char* conn1 = strtok(line, ",");
        char* conn2 = strtok(NULL, ",");
        char* conn3 = strtok(NULL, ",");
        // We need to convert the strings to integers.
        int conn1int = atoi(conn1);
        int conn2int = atoi(conn2);
        int conn3int = atoi(conn3);
        // We need to put the integers into an array.
        int connections[3] = {conn1int, conn2int, conn3int};
        // We need to loop through the array and add the connections to the rooms.
        for (int j = 0; j < numberOfConnections; j++) {
            // The j value refers to the different room choice that the user can selection.
            roomToConnect = connections[j];
            // We need to create a connection between the jth room choice and the current room.
            connection connection;
            // We need to set the room1 value to the current room.
            connection.room1 = &rooms[i];
            // We need to set the room2 value to the room that the user can move to.
            connection.room2 = &rooms[roomToConnect];
            // We need to add the connection to the room
            rooms[i].connections = realloc(rooms[i].connections, sizeof(connection) * (rooms[i].numConnections + 1));
            rooms[i].connections[rooms[i].numConnections] = connection;
            rooms[i].numConnections++;
        }
    }
// we need to return the array of rooms
  return rooms;
}

// let's have a function to create a player
player* createPlayer(room* currentRoom) {
  // we need to allocate memory for the player
  player* player = malloc(sizeof(player));
  // we need to set the current room
  player->currentRoom = currentRoom;
  // we need to return the player
  return player;
}

// let's have a function to create a game
game* createGame() {
  // we need to allocate memory for the game
  game* game = malloc(sizeof(game));
  game->rooms = loadRooms();
  game->numRooms = 10;
  // we need to create the player
  game->player = createPlayer(&game->rooms[0]); // This creates the player with the currentRoom being the first room.

  // we need to return the game
  return game;
}

// let's have a function to play the game which is the main function
void playGame() {
  // we need to create the game
  printf("-----WELCOME TO THE GAME-----\n");
  getScore(); // This calls the function that will calculate the score.
  game* game = createGame();
  // we need to print the room description
  printRoomDescription(game->player->currentRoom);
  // we need to loop until the user quits
  bool quit = false;
  while (!quit) {
    // we need to print the menu and get the user choice
    int choice = getMenuChoice();
    // we need to carry out the user choice
    if (choice == 1) {
      // we need to print the room description
      printRoomDescription(game->player->currentRoom);
    } else if (choice == 2) {
      // we need to get the user choice of room to move to
      int choice = getRoomChoice(game->player->currentRoom);
      // we need to move the player to the room
      movePlayer(game->player, choice);
    } else if (choice == 3) {
      // we need to quit
        printf("Thanks for playing!\n");
        getScore(); // This calls the function that will calculate the score.
      quit = true;
    }
  }
}

// let's have a main function to call the playGame function
int main() {
  playGame();
  return 0;
}










