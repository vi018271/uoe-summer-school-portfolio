// Get the button element by its ID
var button = document.getElementById('contactLowerButton');

// Add a click event listener to the button
button.addEventListener('click', function() {
    // Redirect the user to another page
    window.location.href = '../Contact_Page/contactWebsite.html';
});
// Get the button element by its ID
var contactbutton = document.getElementById('contact');

// Add a click event listener to the button
contactbutton.addEventListener('click', function() {
    // Redirect the user to another page
    window.location.href = '../Contact_Page/contactWebsite.html';
});

var ticTacButton = document.getElementById('ticTacToeButton');

ticTacButton.addEventListener('click', function() {
    window.location.href = '../TicTacToe_Page/ticTacToe.html';
})

var snakeButton = document.getElementById('snakeButton');

snakeButton.addEventListener('click', function() {
    window.location.href = '../Snake_Page/snake.html';
});

var gameOfLifeButton = document.getElementById('gameOfLifeButton');

gameOfLifeButton.addEventListener('click', function() {
    window.location.href = "../ConwaysGameOfLife_Page/conwaysGameOfLife.html";
});