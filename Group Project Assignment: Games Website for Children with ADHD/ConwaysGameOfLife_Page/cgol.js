// The grid class will be useful for both games as it is the board that the game is on.
// My idea is to use the grid to generate a corresponding table of same width and height with each cell representing a tile.
// The Tile class will host the data on each cell
// Data flow between html table and grid class will need to be controlled by a controller on each game page

class Tile{
    constructor(grid_x,grid_y,value,next_value,counter){
        this.grid_x = grid_x; // grid x value of tile
        this.grid_y = grid_y; // grid y value of tile
        this.value = value; // Holds the value which represents different component of game, integer
        this.next_value = next_value // Holds what this value will be in the next turn. Can be used in different ways
        
    }
}

class Grid{
    constructor(width,height){
        this.width = width;
        this.height = height;
        let tile_array = [];
        this.tile_array = tile_array;
        this.game_state = false;

        for (let i=0;i<this.width;i++){ // Creates the 2D array of 0's right now.
            // Should be changed to make tile objects instead
            this.tile_array.push([])
            for (let j=0;j<this.height;j++){
                let new_tile = new Tile(i,j,0,0,0)
                this.tile_array[i].push(new_tile)
            }
        }
    }

    getTiles(){
        return this.tile_array;
    }

    changeTileState(){
        let gridx = document.getElementById("xinput").value;
        let gridy = document.getElementById("yinput").value;
        console.log(gridx.toString())
        console.log(gridy.toString())
        let state = this.tile_array[gridx][gridy].value;
        if (state == 0){
        this.tile_array[gridx][gridy].value = 1;
        } 
        if (state == 1){
            this.tile_array[gridx][gridy].value = 0;
        }
    }

    changeTileStateByClick(e){
        let pageid = e.id;
        let coords = pageid.split(".");
        let coords_integer = [parseInt(coords[1])-1,parseInt(coords[0])-1];
        let value = this.tile_array[coords_integer[0]][coords_integer[1]].value;
        if (value == 0){
            this.tile_array[coords_integer[0]][coords_integer[1]].value = 1;
        }
        if (value == 1){
            this.tile_array[coords_integer[0]][coords_integer[1]].value = 0;
        }
        
        
        
    }

    changeSimulationState(){
        this.game_state = !this.game_state;
    }
    
    getNearestNeighbours(grid_x,grid_y){
        let current_tile = this.tile_array[grid_x][grid_y];
        let neighbour_number = 0;
        for (let i = -1;i<2;i++){
            for (let j =-1;j<2;j++){
                if(current_tile.grid_x+i >= 0 && current_tile.grid_y+j >=0 &&  current_tile.grid_x+i<this.tile_array.length && current_tile.grid_y+j<this.tile_array[0].length && !(i==0 && j==0)){
                    if(this.tile_array[grid_x+i][grid_y+j].value == 1){
                        neighbour_number+=1;
                    }
                }
            }
        }
        console.log(neighbour_number);
        return neighbour_number;
    }

    update(){
        for(let i = 0; i < this.tile_array.length;i++){
            for (let j = 0; j< this.tile_array[0].length;j++){
                let current_tile = this.tile_array[i][j];
                let neighbour_amount = this.getNearestNeighbours(current_tile.grid_x,current_tile.grid_y);
                // Game Of Life Logic
                current_tile.next_value = current_tile.value;
                
                if(neighbour_amount<2){
                    //Underpopulated tile dies
                    current_tile.next_value = 0;
                }
                else if(neighbour_amount==2){
                    if (current_tile.value == 1){
                        current_tile.next_value = 1;
                    }
                }
                else if(neighbour_amount==3){
                    //Tile gets populated
                    current_tile.next_value = 1;
                }
                else{
                    // Tile is overcrowded
                    current_tile.next_value = 0;
                }
                
            }
        }

        for(let i = 0; i < this.tile_array.length;i++){
            for (let j = 0; j< this.tile_array[0].length;j++){
                let current_tile = this.tile_array[i][j];
                current_tile.value = current_tile.next_value;
            }
        }

    }
}

function updateDisplay(gameGrid){
    let sentence = "";
    for (let i = 0; i<gameGrid.getTiles().length;i++){ // Loop through 1D array in 2D array
        for (let j = 0; j<gameGrid.getTiles()[i].length;j++){ // Loop through all tiles 
            id = (j+1).toString()+"."+(i+1).toString(); // Get the ID corresponding to the tile in html
            // Then check what value tile is and set corresponding colour to html element with id
            if (gameGrid.getTiles()[i][j].value == 0){ 
            document.getElementById(id).style.backgroundColor = "black";
            }
            
            if (gameGrid.getTiles()[i][j].value == 1){
            document.getElementById(id).style.backgroundColor = "white";
            }
            
            if (gameGrid.getTiles()[i][j].value == 2){
            document.getElementById(id).style.backgroundColor = "red";
            }
        }
        
    }
    return sentence;
}

function updateGame(gameGrid){
    // Called by gameLoop when enough time passed at fixed intervals to check game logic and then redraw the screen
    if (gameGrid.game_state){
        gameGrid.update()
    }
    updateDisplay(gameGrid)
}

function goHome() {
    const goHomeButton = document.getElementById('back');

    goHomeButton.addEventListener('click', function() {
        window.location.href = '../Home_Page/mainWebsite.html';
    })
}