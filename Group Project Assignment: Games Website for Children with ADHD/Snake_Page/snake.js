// The grid class will be useful for both games as it is the board that the game is on.
// My idea is to use the grid to generate a corresponding table of same width and height with each cell representing a tile.
// The Tile class will host the data on each cell
// Data flow between html table and grid class will need to be controlled by a controller on each game page


class Tile{
    constructor(grid_x,grid_y,value){
        this.grid_x = grid_x; // grid x value of tile
        this.grid_y = grid_y; // grid y value of tile
        this.value = value; // Holds the value which represents different component of game, integer

    }
    getPos(){ // Returns the position of the tile in the grid
        return [this.grid_x,this.grid_y]    
    }
    setVal(val){ // Sets the value of the tile
        this.value = val; 
    }
    getVal(){ // Returns the value of the tile
        return this.value;
    }
}

class Grid{
    constructor(width,height){
        this.width = width;
        this.height = height;
        let tile_array = [];
        this.tile_array = tile_array

        for (let i=0;i<this.width;i++){ // Creates the 2D array of 0's to represent the grid
            this.tile_array.push([])
            for (let j=0;j<this.height;j++){
                let new_tile = new Tile(i,j,0,0) // Create a new tile with x and y values
                this.tile_array[i].push(new_tile) // Push the new tile into the 2D array
            }
        }
    }

    getTiles(){
        return this.tile_array;
    }
}

class Snake{
    //Snake class will create an array of tiles that will represent the snake
    //We will use the tile class to create the snake
    constructor(){
        this.snake_array = [];
        this.length = 3;
        this.direction = "r"; //r is right, l is left, u is up, d is down
        this.score = 0;
        }

    //Get the score of the snake
    getScore(){
        return this.score;
    }

    //Create the snake on the grid
    hook(gameGrid){
        //Loop depending on the length of the snake
        for (let i = 0; i<this.length;i++){
            this.snake_array.push(gameGrid.tile_array[i][0]);
            gameGrid.tile_array[i][0].setVal(1);
        }
    }

    //Create a function to update the snake
    update(){
        this.moveSnake();
    }

    //Create a function to move the snake
    moveSnake(){
        let head = this.snake_array[this.snake_array.length-1];
        let pos = head.getPos();
        let next_tile;
        
        //Check for out of bounds, and if so end the game
        if (pos[0] == 0 && this.direction == "l"){
            gameOver();
        }
        else if (pos[0] == gameGrid.width-1 && this.direction == "r"){
            gameOver();
        }
        else if (pos[1] == 0 && this.direction == "u"){
            gameOver();
        }
        else if (pos[1] == gameGrid.height-1 && this.direction == "d"){
            gameOver();
        }

        //Check which direction the snake is moving and then move the snake
        if (this.direction == "r"){
            next_tile = gameGrid.tile_array[pos[0]+1][pos[1]];
        }
        else if (this.direction == "l"){
            next_tile = gameGrid.tile_array[pos[0]-1][pos[1]];
        }
        else if (this.direction == "u"){
            next_tile = gameGrid.tile_array[pos[0]][pos[1]-1];
        }
        else if (this.direction == "d"){
            next_tile = gameGrid.tile_array[pos[0]][pos[1]+1];
        }

        //Check for snake collision
        if (next_tile.value == 1){
            gameOver();
        }
        //Check for apple collision
        if (next_tile.value == 2){
            next_tile.setVal(1);
            this.snake_array.push(next_tile);
            //Spawn a new apple
            apple.spawn(gameGrid);
            this.score += 1;
            var score = document.getElementById("score");
            score.innerHTML = "Score: "+this.score; //Update the score on the screen
        }
        //If empty tile move the snake
        else if (next_tile.value == 0){
            //Pop the tail
            this.snake_array[0].setVal(0);
            this.snake_array.shift();
            //Move the snake
            next_tile.setVal(1);
            this.snake_array.push(next_tile);
        }
    }

    //Create a function to change the direction of the snake
    changeDirection(direction){
        this.direction = direction;
    }
}

class Apple {
    constructor(){
        this.x = 0;
        this.y = 0;
    }
    spawn(gameGrid){
        //Create a function to spawn the apple
        let x = Math.floor(Math.random()*gameGrid.width); //Get a random x value
        let y = Math.floor(Math.random()*gameGrid.height); //Get a random y value
        let tile = gameGrid.tile_array[x][y];
        //Check if the apple is spawning on the snake
        if (tile.getVal() == 1){
            this.spawn(gameGrid); //If so, spawn the apple again
        }
        else{
            tile.setVal(2); //Set the value of the tile to 2 (apple)
        }
    }       
}

 function updateDisplay(gameGrid){
    let sentence = "";
    for (let i = 0; i<gameGrid.getTiles().length;i++){ // Loop through 1D array in 2D array
        for (let j = 0; j<gameGrid.getTiles()[i].length;j++){ // Loop through all tiles 
            id = (j+1).toString()+"."+(i+1).toString(); // Get the ID corresponding to the tile in html
            // Then check what value tile is and set corresponding colour to html element with id
                    if (gameGrid.getTiles()[i][j].value == 0){
                    document.getElementById(id).style.backgroundColor = "#7be37b";
                    }
                    
                    if (gameGrid.getTiles()[i][j].value == 1){
                    document.getElementById(id).style.backgroundColor = "blue";
                    }
                    
                    if (gameGrid.getTiles()[i][j].value == 2){
                    document.getElementById(id).style.backgroundColor = "red";
                    }
                }
                
            }
            return sentence;
        }

function updateGame(gameGrid,snake){
    // Called by gameLoop when enough time passed at fixed intervals to check game logic and then redraw the screen
    snake.update(gameGrid);
    updateDisplay(gameGrid);
}

function restartGame(){ // Function to restart the game
    location.reload(); 
}

function gameOver(){ // Function to end the game
    var element = document.getElementById("gameOver");
    element.style.visibility = "visible";
    //Edit the game over message to show the current score
    var score = document.getElementById("gameOverScore");
    score.innerHTML = "Your score was: "+snake.getScore();
    document.addEventListener('keydown', function(event) { 
        if (event.code == 'Enter') { // If enter is pressed restart the game
            restartGame();
        }
    });
}

var goHomeButton = document.getElementById('back'); // Get the back button

goHomeButton.addEventListener('click', function() {
    window.location.href = '../Home_Page/mainWebsite.html'; // Redirect to home page
})