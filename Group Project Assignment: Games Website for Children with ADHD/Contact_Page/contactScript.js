const openButton = document.getElementById("submitButton");
const closeButton = document.getElementById("closeModal");
const modal = document.getElementById("modal");

const emailInput = document.getElementById('emailInput');
const messageInput = document.getElementById('messageInput');

const closeErrorButton = document.getElementById('closeError');

openButton.addEventListener("click", () => {
    const emailInputValue = emailInput.value.trim();
    const messageInputValue = messageInput.value.trim();
    if ((emailInputValue === "") || (messageInputValue === "")) {
        // If one or more fields are empty.
        error.classList.add("open"); // Opens the error pop-up
    } else {
        // If both fields contain data.
        const containsAtSymbol = /@/.test(emailInputValue); // Testing if the email input contains a @
        if (containsAtSymbol) {
            // Email Input contains an @ symbol
            modal.classList.add("open"); // Opens the pop-up
            console.log("The email is: " + emailInputValue + " and the message is: " + messageInputValue);
            // The message is logged in the console so that there is a record of it.
            // I would have used Node JS to write this data to a file but believed this is beyond the scope of this project.
        } else {
            error.classList.add("open"); // Opens the error pop-up
        }
    }
});


closeButton.addEventListener("click", () => {
    modal.classList.remove("open"); // Closes the pop up
    window.location.href = '../Home_Page/mainWebsite.html'; // Directs the user to the homepage
});

closeErrorButton.addEventListener("click", () => {
    error.classList.remove("open"); // Closes the pop up
});


