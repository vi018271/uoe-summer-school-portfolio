#include "characterClass.h"

#include <string>
#include <iostream>

#pragma once

class Player; // Forward declaration of the player class.
class NPC; // Forward declaration of the NPC class.

class commandInterpreter {
private:
    Player* player_;
    NPC* raven; // Forward declaration of the Raven NPC.
    NPC* thorn; // Forward declaration of the Thorn NPC.
    NPC* sable; // Forward declaration of the Sable NPC.
public:
    commandInterpreter(Player* player, NPC* raven, NPC* thorn, NPC* sable) : player_(player), raven(raven), thorn(thorn), sable(sable) {} // Constructor
    void InterpretCommand(const std::string& command); //
};

