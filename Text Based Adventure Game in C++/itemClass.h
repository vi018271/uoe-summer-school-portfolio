#include <iostream>
#include <string>

#pragma once

class Item {
private:
    std::string name;
    std::string description;
public:
    Item(const std::string& name, const std::string& desc):description(desc),name(name){};
    void Interact();
    std::string GetName();
    void SetName(const Item &item, std::string newName);
    std::string GetDescription();
};
