import json


def load_json(file): # This function loads the JSON file and opens it for use in the program.
    with open(file) as bot_responses:
        return json.load(bot_responses) # Returning the loaded JSON file back to the main program.

def get_response(user_input):
    data = load_json("bot.json") # Storing the JSON file in the data variable.
    for response in data:
        if 'user-input' in response:
            if user_input.lower() in response['user-input']:
                return response['bot-response']
    return "I have travelled many realms ... but I have not gained this knowledge yet!"

while True:
    user_input = input("You: ")
    response = get_response(user_input)
    print("Them: ", response)
    
