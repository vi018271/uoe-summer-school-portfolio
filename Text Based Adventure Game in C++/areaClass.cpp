#include "areaClass.h"
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

void Area::AddRoom(const std::string& name, Room* room) {
    rooms[name] = room; // The name of the room will be the name as a string and the room will be a pointer for the name of the room.
}

Room* Area::GetRoom(const std::string& name) {
    return rooms[name]; // This returns a nullptr for the pointer room1 and room2 - WHY!
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    Room* room1 = GetRoom(room1Name); // Retrieving the room pointer for the first room.
    Room* room2 = GetRoom(room2Name); // Retrieving the room pointer for the second room.
    if (room1 && room2) {
        // If the pointers for both rooms are retrieved successfully.
        room1 ->AddExit(direction, room2);
    } else {
        // If an error occurs retrieving the room pointers.
        std::cerr << "Error: One or more of the rooms were not found!" << std::endl;
    }
}

void Area::LoadMapFromFile(const std::string& filename) {
    // I need to open the file which stores the map using ifstream.
    std::ifstream myFile(filename);
    std::string myLine; // Stores each line in the file
    // The below line iterates through the file and creates a connection based on each line in the file
    while (std::getline(myFile,myLine)) {
        std::vector<std::string> elements;
        std::istringstream iss(myLine);
        std::string element;
        while (std::getline(iss, element, '|')) {
            elements.push_back(element); // Pushes each segment in the line onto the vector.
        }
        // A vector is a dynamic array which means the first element appended to the vector is the first element.
        // We know that once the while loop has iterated a given line in the file - there will be 3 elements in the vector.
        std::string r1 = elements.front(); // Storing the first element in the vector (the current room in r1)
        elements.erase(elements.begin()); // Removes the first element in the vector.
        std::string r2 = elements.front(); // Storing the second element in the vector (the room to move to in r2)
        elements.erase(elements.begin()); // Removes the second element in the vector.
        std::string direction = elements.front(); // Storing the third element in the vector (the direction to move to get from r1 to r2)
        elements.erase(elements.begin()); // Removes the second element in the vector
        // Now I have to call the subroutine that creates a connection between the two rooms.
        ConnectRooms(r1, r2, direction);
    }
}
