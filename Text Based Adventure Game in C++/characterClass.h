
#include <iostream>
#include <string>
#include <vector>
#include "areaClass.h"

#pragma once

class Character {
private:
    std::vector<Item> inventory;
protected:
    std::string name;
    int health;
public:
    Character(std::string& mname, int mhealth):name(mname),health(mhealth){};
    void TakeDamage(int damage);
};
class Player {
private:
    Room* location;
    std::string name;
    int health;
    std::vector<Item> bag; // This stores the items in the player's bag.
    std::vector<Item> belt; // This stores the items on the player's belt.
public:
    Player(std::string& mname, int mhealth):name(mname),health(mhealth){};
    Room* SetLocation(Room *room);
    Room* GetLocation();
    void move(std::string& direction);
    void AddToInventory(std::string& itemName);
    void GetItemFromBag();
    void GetItemFromBelt();
};

class NPC : public Character {
private:
    std::string description;
public:
    NPC(std::string& myName, int myHealth, std::string& myDescription): Character(myName, myHealth), description(myDescription) {}; // Constructor
    std::string GetName();
    std::string GetDescription();
    void chat();

};
