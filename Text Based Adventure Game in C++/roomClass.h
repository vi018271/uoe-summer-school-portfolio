#include "itemClass.h"

#pragma once

#include <iostream>
#include <string>
#include <map>
#include <vector>

class NPC; // Forward declaration of the NPC class.

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits; // Change to hold pointers to Room objects
    std::vector<Item> items; // This stores the items in a given room.
    std::vector<NPC*> npcs; // This stores a pointer to the NPC which is in a given room.
public:
    Room(const std::string& desc):description(desc){};
    void AddItem(const Item& item);
    void RemoveItem(const Item& item);
    void AddExit(const std::string& key, Room* value); // Change to Room*
    std::vector<Item> GetItems();
    Room* GetExit(const std::string& direction); // Change return type to Room*
    std::string GetRoomDescription();
    void AddNPC(NPC* npc);
    std::vector<NPC*> GetNPC();
};
