#include <iostream>
#include "roomClass.h"

#pragma once

class Area {
private:
    std::map<std::string, Room*> rooms; // Holds room objects where the string key is the room's name
public:
    Area(){} // Default Constructor
    void AddRoom(const std::string& name, Room* room); // Adds a room to the area using its name as a key.
    Room* GetRoom(const std::string& name); // Retrieves a room by its name.
    void ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction);
    // The above declaration connects two rooms using a specified direction (e.g. north)
    void LoadMapFromFile(const std::string& filename); // Loads the game map from a text file, creating rooms and connections.
};
