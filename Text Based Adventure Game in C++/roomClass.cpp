#include "roomClass.h"

void Room::AddItem(const Item &item) {
    // Here we are creating the AddItem method within the Room class which is defined in the roomClass.h header file.
    items.push_back(item);
}

void Room::RemoveItem(const Item &item) {
    // Here we are creating the RemoveItem method within the Room class which is defined in the roomClass.h header file.
    // Assuming the item we have passed in is the index of the item to remove.
    items.pop_back(); // THIS POPS THE TOP ITEM - HOWEVER THIS NEEDS TO CHANGE AS WE ARE NOT REMOVING THE SPECIFIC INDEX.
}

void Room::AddExit(const std::string& key, Room* value) {
    // Here we are appending an exit to the map data structure
    exits[key] = value;
}

std::vector<Item> Room::GetItems() {
    return items;
}

Room* Room::GetExit(const std::string& direction) {
    return exits[direction];
}

std::string Room::GetRoomDescription() {
    return description;
}

void Room::AddNPC(NPC* npc) {
    npcs.push_back(npc);
}

std::vector<NPC*> Room::GetNPC() {
    // This is a function that gets the NPC that is in the room that the user is currently in.
    return npcs;
}
