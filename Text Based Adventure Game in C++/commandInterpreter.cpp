#include "commandInterpreter.h"
#include <string>
#include <sstream>


#include "commandInterpreter.h"
#include <string>
#include <sstream>

void commandInterpreter::InterpretCommand(const std::string &command) {
    // There are several possible commands that the user can enter to cause a change in state.
    bool isMove =
            command.find("move") != std::string::npos; // Checks if the user wants to move in a specified direction.
    bool isLook = command.find("look") != std::string::npos; // Checks if the user wants to look at an item.
    bool isPick = command.find("pick") !=
                  std::string::npos; // Checks if the user wants to pick up an item and add it to their inventory.
    bool isCheck = command.find("check") !=
                   std::string::npos; // Checks if the user wants to check the contents of their specified inventory.
    bool isChat =
            command.find("chat") != std::string::npos; // Checks if the user wants to chat to the NPC in their room.

    if (isMove) {
        // Now we know that the user wants to move in a direction - we need to break down the string to get the direction.
        std::istringstream iss(command); // Splits the sentence around spaces.
        std::string word; // Used for storing each word
        iss >> word; // Skipping the first word in the sentence
        if (iss >> word) {
            std::string directionToMove = word;
            // We now need to move the player in the direction specified.
            // To do this we will have a function in the Player class which moves the user.
            player_->move(directionToMove);
        }
    }

    if (isLook) {
        // There is one item in every room therefore we can get the description of the item based on the user's current location.
        for (Item &item: player_->GetLocation()->GetItems()) {
            std::cout << item.GetDescription() << std::endl;
        }
    }

    if (isPick) {
        // Firstly, we need to get the name of the item the user wants to add to their inventory.
        std::istringstream iss(command); // Splits the sentence about the spaces.
        std::string word; // Used for storing each word.
        iss >> word; // Skipping the first word in the sentence.
        iss >> word; // Skipping the second word in the sentence.
        if (iss >> word) {
            std::string itemName = word;
            // Now we need to check if the itemName corresponds to a valid item in the room.
            bool itemFound = false;
            for (Item &item: player_->GetLocation()->GetItems()) {
                if (item.GetName() == itemName) {
                    // This means the item name specified corresponds to the item in the current room.
                    player_->AddToInventory(itemName);
                    itemFound = true;
                    break; // No need to continue searching
                }
            }
            if (!itemFound) {
                std::cout << "I am sorry but that item does not exist!" << std::endl; // Error message.
            }
        }
    }

    if (isCheck) {
        // Firstly we need to get the name of the inventory that the user wants to check.
        std::istringstream iss(command); // Splits the sentence about the spaces.
        std::string word; // Used for storing each word.
        iss >> word; // Skipping the first word in the sentence
        if (iss >> word) {
            std::string inventoryName = word;
            // Now we need to check which inventoryName the user entered
            if ((inventoryName == "bag") || (inventoryName == "Bag") || (inventoryName == "BAG")) {
                // If the player wants to check their bag.
                player_->GetItemFromBag();
            }
            if ((inventoryName == "belt") || (inventoryName == "Belt") || (inventoryName == "BELT")) {
                // If the user wants to check their belt.
                player_->GetItemFromBelt();
            }
        }
    }
    if (isChat) {
        // All NPCs will have the same conversation so I can just direct flow to the chat function of one.
        raven->chat();
    }
}
