#include "characterClass.h"
#include <string>
#include <vector>
#include <Python.h>

void Character::TakeDamage(int damage) {
    int currentHealth = Character::health;
    int newHealth = currentHealth - damage;
    std::cout << "You have taken Damage - Your Health is currently: " << damage;
}

Room* Player::SetLocation(Room* currentRoom) {
    Player::location = currentRoom;
    return currentRoom;
}

Room* Player::GetLocation() {
    return Player::location;
}

void Player::move(std::string& direction) {
    // Player moves to another room
    Room* nextRoom = Player::location -> GetExit(direction);
    if (nextRoom != nullptr) {
        SetLocation(nextRoom);
        std::cout << "You move to the next room." << std::endl;
    } else {
        std::cout << "You can't go that way." << std::endl;
    }
}

void Player::AddToInventory(std::string& itemName) {
    // We need to find out which inventory the user is going to put the item in.
    std::cout << "Do you want to put the item in your bag or on your belt?  Please enter your choice as a single word in all lowercase "
                 "characters." << std::endl;
    // cin statements stop reading the input when a white space is found so if the input is more than one word - it would
    // only store the first word of the sentence in the choice variable.
    std::string choice;
    std::cin >> choice;
    if (choice == "bag") {
        // If the bag inventory is selected by the user.
        for (Item& item : Player::GetLocation()->GetItems()) {
            bag.push_back(item); // Pushes the item onto the bag.
            std::cout << "The " << item.GetName() << " has been added to your bag!" << std::endl;
        }
    }
    else if (choice == "belt") {
        // If the belt inventory is selected by the user.
        for (Item& item : Player::GetLocation()->GetItems()) {
            belt.push_back(item); // Pushes the item onto the bag.
            std::cout << "The " << item.GetName() << " has been added to your bag!" << std::endl;
        }
    } else {
        // If the input does not correspond to a valid inventory.
        std::cout << "There is no inventory called " << choice << ".  Please choose between bag and belt." << std::endl;
    }
}

void Player::GetItemFromBag() {
    // We need to iterate through the vector and print each item in the vector.
    // First we need to check if the vector is empty.
    if (bag.empty()) {
        std::cout << "Your bag is empty.  Find some items to put into it!" << std::endl;
    } else {
        // Now we need a for loop to iterate through the items in the vector.
        std::cout << "Your bag contains:" << std::endl;
        for (size_t i = 0; i < bag.size(); i++) {
            std::cout << "- " << bag[i].GetName() << std::endl;
        }
    }
}

void Player::GetItemFromBelt() {
    // We need to iterate through the vector and print each item in the vector.
    // First we need to check if the vector is empty.
    if (belt.empty()) {
        std::cout << "Your belt is empty.  Find some items to put onto it!" << std::endl;
    } else {
        // Now we need a for loop to iterate through the items in the vector.
        std::cout << "Your belt contains:" << std::endl;
        for (size_t j = 0; j < belt.size(); j++) {
            std::cout << "- " << belt[j].GetName() << std::endl;
        }
    }
}

std::string NPC::GetName() {
    return name;
}

std::string NPC::GetDescription() {
    return description;
}

void NPC::chat() {
    // This is the function that allows NPCs to interact with the user using the JSON and Python file.
    const char* fileName = "../ChatBot.py"; // Storing the name of the file in a variable
    Py_Initialize(); // Initialising the Python Interpreter
    // Running the Python Script from the ChatBot.py File
    FILE* file = fopen(fileName, "r"); // Opening the file to read from it
    if (file != nullptr) { // Checking if the file is empty.
        PyRun_SimpleFile(file, fileName); // Running the code in the file using the Python.h header file.
        fclose(file); // Closing the file.
    } else {
        std::cerr << "Failed to open the Python Script."; // Raising an error if a problem has occurred.
    }
    Py_Finalize(); // Finalising the Python Interpreter
}