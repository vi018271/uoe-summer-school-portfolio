#include "commandInterpreter.h"
#include <iostream>

int main() {
    // Create Rooms
    Room startRoom("You are in a dimly lit room.");
    Room hallway("You are in a long hallway.");
    Room treasureRoom("You have entered a treasure room!");
    // Create an instance of the Area class
    Area gameWorld;
    // There are three rooms that always exist in this program: startRoom, hallway and treasureRoom
    // When the program is run they are instantiated and I need to append them to the rooms map in the Area Class
    gameWorld.AddRoom("startRoom", &startRoom);
    gameWorld.AddRoom("hallway", &hallway);
    gameWorld.AddRoom("treasureRoom", &treasureRoom);
    // Load the game map from a text file
    gameWorld.LoadMapFromFile("game_map.txt");
    // Create Items
    Item key("Key", "A shiny key that looks important.");
    Item sword("Sword", "A sharp sword with a golden hilt.");
    // Add the Items to the Rooms
    startRoom.AddItem(key);
    treasureRoom.AddItem(sword);
    // Creating the NPCs
    std::string npc1Name = "Raven";
    std::string npc1Description = "a shadowy figure draped in midnight hues, Raven's eyes betray depths of hidden knowledge.";
    NPC Raven(npc1Name, 100, npc1Description);
    std::string npc2Name = "Thorn";
    std::string npc2Description = "clad in battle-worn armor, Thorn's presence speaks of trials endured and victories claimed.";
    NPC Thorn(npc2Name, 100, npc2Description);
    std::string npc3Name = "Sable";
    std::string npc3Description = "cloaked in celestial robes, Sable's gaze holds the wisdom of ages past, guiding seekers through life's labyrinth.";
    NPC Sable(npc3Name,100, npc3Description);
    // Adding the NPCs to Rooms
    startRoom.AddNPC(&Raven);
    hallway.AddNPC(&Thorn);
    treasureRoom.AddNPC(&Sable);
    // Create a Player
    std::string name = "Alice";
    Player player(name, 100);
    // Set the player's starting room (you can modify this room name)
    Room *currentRoom = gameWorld.GetRoom("startRoom");
    player.SetLocation(currentRoom);
    // Creating the Command Interpreter
    commandInterpreter interpreter(&player, &Raven, &Thorn, &Sable);
    // Game loop
    while (true) {
        // Getting the items in the user's current room.
        std::cout << "Items in the room:" << std::endl;
        for (Item &item: player.GetLocation()->GetItems()) {
            std::cout << "- " << item.GetName() << std::endl;
        }
        // Getting the exits available in the user's current room.
        std::cout << "Exits available: ";
        // There are two possible directions that the user can move from a given location: north and south.
        for (const auto &direction: {"north", "south"}) {
            Room *exitRoom = player.GetLocation()->GetExit(direction);
            if (exitRoom != nullptr) {
                std::cout << direction << " " << std::endl;
            }
        // Need to create a loop which finds the NPC that is currently in the room with the user.
        std::vector <NPC*> npcInRoom = player.GetLocation()->GetNPC();
            NPC* myNPC = npcInRoom[0];
            std::cout << "You've encountered " << myNPC->GetName() << ", " << myNPC -> GetDescription() << std::endl;
        std::cout << "\n" << std::endl;
        std::cout << "---------------------------------------------POSSIBLE ACTIONS:---------------------------------------------" << std::endl;
        std::cout << "move (direction)                  This moves the player from the current room in the direction specified." << std::endl;
        std::cout << "look at (item name)               This allows the user to look at the item in more detail." << std::endl;
        std::cout << "pick up (item name)               This stores the item specified in the player's inventory for later use." << std::endl;
        std::cout << "check (inventory name)            This displays the items stored in the player's inventory that is specified." << std::endl;
        std::cout << "chat                              This allows the user to chat to " << myNPC -> GetName() << "." << std::endl;
        std::cout << "Please enter your desired action: " << std::endl;
        std::string choice;
        std::getline(std::cin, choice); // Having to use getLine as cin does not take note of whitespace
        interpreter.InterpretCommand(choice);
        break;
        }
    }
}